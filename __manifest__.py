# -*- coding: utf-8 -*-
{
    'name': "Silaris",

    'summary': """
        I believe every human has a finite number of heartbeats.""",

    'description': """
        You know, being a test pilot isn't always the healthiest business in the world.
        If you could see the earth illuminated when you were in a place as dark as night, it would look to you more splendid than the moon.
    """,

    'author': "FPS",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Accounting',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'purchase', 'stock'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/customerinvoice.xml',
        'views/deliveryslip.xml',
        'views/faktur.xml',
        'views/header.xml',
        'views/journalitems.xml',
        'views/partner.xml',
        'views/purchasedetail.xml',
        'views/purchaseorder.xml',
        'views/resources.xml',
        'views/saledetail.xml',
        'views/salesummary.xml',
        'views/saleorder.xml',
        'views/salesarea.xml',
        'views/stock.xml',
        'views/templates/deliveryslip.xml',
        'views/templates/footer.xml',
        'views/templates/header.xml',
        'views/templates/invoice.xml',
        'views/templates/purchaseorder.xml',
        'views/templates/saleorder.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}