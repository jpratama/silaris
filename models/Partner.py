# -*- coding: utf-8 -*-

from odoo import models, fields

class Partner(models.Model):

    _inherit = 'res.partner'
    salesarea = fields.Many2one('silaris.salesarea', 'Sales Area')
    businessunit = fields.Many2one('account.analytic.account', 'Business Unit')
    no_ktp = fields.Char('No. KTP')
    npwp = fields.Char('NPWP')