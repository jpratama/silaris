# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools

class PurchaseDetail(models.Model):
    _name = 'silaris.purchase_detail'
    _description = 'Purchase order detail'
    _auto = False
    _order = 'po_number DESC'

    po_schedule = fields.Date('Schedule Date', readonly=True)
    inv_date = fields.Date('Bill Date', readOnly=True)
    po_number = fields.Char('PO Number', readonly=True)
    po_source = fields.Char('Source Number', readOnly=True)
    dn_number = fields.Char('DN Number', readonly=True)
    inv_number = fields.Char('Bill Number', readonly=True)
    inv_amount_untaxed = fields.Float('Amount Untaxed (Inv)', readonly=True)
    inv_amount_total = fields.Float('Total (Inv)', readonly=True)
    vendor_ref = fields.Char('Vendor Reference', readonly=True)
    vendor_name = fields.Char('Vendor Name', readonly=True)
    item = fields.Char('Item', readonly=True)
    billed_qty = fields.Integer('Billed Qty', readonly=True)
    unit_price = fields.Float('Unit Price', readonly=True)
    amount_untaxed = fields.Float('Amount Untaxed', readonly=True)
    tax = fields.Float('Tax', readonly=True)

    def _query(self):
        query_str = """
            SELECT row_number() over () as id,
                po.date_planned as po_schedule,
                inv.date_invoice as inv_date,
                po.name as po_number,
                po.origin as po_source,
                dn.name as dn_number,
                inv.number as inv_number,
                inv.reference as vendor_ref,
                inv.amount_untaxed as inv_amount_untaxed,
                inv.amount_total as inv_amount_total,
                partner.name as vendor_name,
                po_line.name as item,
                po_line.qty_invoiced as billed_qty,
                po_line.price_unit as unit_price,
                po.amount_untaxed as amount_untaxed,
                po.amount_tax as tax

            FROM purchase_order po
                LEFT JOIN account_invoice inv ON inv.origin = po.name OR inv.origin LIKE '%' || po.name || '%'
                LEFT JOIN stock_picking dn ON dn.origin = po.name
                LEFT JOIN res_partner partner ON partner.id = po.partner_id
                INNER JOIN purchase_order_line po_line ON po_line.order_id = po.id
            
            WHERE dn.state != 'cancel'

            ORDER BY po_number DESC
        """
        return query_str

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'silaris_purchase_detail')
        self._cr.execute("CREATE OR REPLACE VIEW silaris_purchase_detail AS (" + self._query() + ")")
