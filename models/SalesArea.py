# -*- coding: utf-8 -*-

from odoo import models, fields

class SalesArea(models.Model):

    _name = 'silaris.salesarea'
    name = fields.Char('Sales Area', required=True)
    description = fields.Text()