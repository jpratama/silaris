# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools

class SaleSummary(models.Model):
    _name = 'silaris.sale_summary'
    _description = 'Sales Summary Report'
    _auto = False
    _order = 'sales_person ASC'

    business_unit = fields.Char('Business Unit', readonly=True)
    sales_person = fields.Char('Sales Person', readOnly=True)
    product_name = fields.Char('Product Name', readonly=True)
    customer_name = fields.Char('Customer Name', readonly=True)
    delivered_quantity = fields.Integer('Delivered Quantity', readonly=True)
    amount_untaxed = fields.Float('Amount Untaxed', readonly=True)

    def _query(self):
        query_str = """
            SELECT ROW_NUMBER
                () OVER () AS id,
                business_unit,
                sales_person,
                dn_product_name AS product_name,
                customer_name,
                SUM ( dn_quantity ) AS delivered_quantity,
                SUM (so_unit_price * dn_quantity) AS amount_untaxed 
            FROM
                silaris_sale_detail 
            GROUP BY
                business_unit,
                sales_person,
                dn_product_name,
                customer_name
        """
        return query_str

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'silaris_sale_summary')
        self._cr.execute("CREATE OR REPLACE VIEW silaris_sale_summary AS (" + self._query() + ")")