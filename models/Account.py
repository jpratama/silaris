# -*- coding: utf-8 -*-

from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

class Account(models.Model):

    _inherit = 'account.move.line'
    invoice_date = fields.Date('Invoice Date', compute='_get_invoice_date')

    @api.one
    def _get_invoice_date(self):
        self.invoice_date = self.invoice_id.date_invoice

class Invoice(models.Model):
    _inherit = 'account.invoice'
    delivery_number = fields.Char('Delivery Slip No.', compute='_get_delivery_number')
    businessunit = fields.Char('Business Unit', store=True, compute='_get_business_unit')

    @api.one
    def _get_delivery_number(self):
        model_name = 'sale.order'
        if self.type == 'in_invoice':
            model_name = 'purchase.order'

        order = self.env[model_name].search([('name', '=', self.origin)])
        picking_list = []
        for picking in order.picking_ids:
            picking_list.append(picking.name)
        self.delivery_number = ", ".join(picking_list)

    @api.one
    def _get_business_unit(self):
        self.businessunit = self.partner_id.businessunit.name