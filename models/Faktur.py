# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools

class Faktur(models.Model):
    _name = 'silaris.faktur'
    _description = 'Data E-Faktur'
    _auto = False
    _order = 'number desc'

    number = fields.Char('Invoice Number', readonly=True)
    date = fields.Date('Invoice Date', readonly=True)
    state = fields.Char('Invoice State', readonly=True)
    customer = fields.Char('Customer', readonly=True)
    ktp = fields.Char('KTP', readonly=True)
    npwp = fields.Char('NPWP', readonly=True)
    sales_order = fields.Char('Sales Order', readonly=True)
    customer_reference = fields.Char('Customer Ref.', readonly=True)
    item = fields.Char('Item', readonly=True)
    quantity = fields.Float('Quantity', readonly=True)
    amount_untaxed = fields.Float('Amount Untaxed', readonly=True)
    amount_tax = fields.Float('Tax', readonly=True)
    amount_due = fields.Float('Amount Due', readonly=True)
    street = fields.Char('Street', readonly=True)
    street2 = fields.Char('Street 2', readonly=True)
    city = fields.Char('City', readonly=True)
    zip = fields.Char('Zip', readonly=True)

    # Contoh query for view/report: https://github.com/odoo/odoo/blob/10.0/addons/sale/report/sale_report.py
    def _query(self):
        query_str = """
            SELECT line.id as id,
                i.number as number,
                i.date_invoice as date,
                i.state as state,
                p.name as customer,
                p.no_ktp as ktp,
                p.npwp as npwp,
                p.street,
                p.street2,
                p.city,
                p.zip,
                s.name as sales_order,
                s.client_order_ref as customer_reference,
                line.name as item,
                line.quantity as quantity,
                line.price_subtotal as subtotal,
                i.amount_untaxed as amount_untaxed,
                i.amount_tax as amount_tax,
                i.amount_total as amount_total,
                i.residual as amount_due
                
            FROM account_invoice i LEFT JOIN account_invoice_line line ON i.id = line.invoice_id
                JOIN res_partner p on i.partner_id = p.id
                JOIN sale_order s on i.origin = s.name

            WHERE i.type = 'out_invoice'
                AND (i.state = 'paid' OR i.state = 'open')
        """
        return query_str

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'silaris_faktur')
        self._cr.execute("CREATE OR REPLACE VIEW silaris_faktur AS (" + self._query() + ")")