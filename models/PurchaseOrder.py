# -*- coding: utf-8 -*-

from odoo import models, fields, api

import logging
_logger = logging.getLogger(__name__)

class PurchaseOrder(models.Model):

    _inherit = 'purchase.order'
    arrival_status = fields.Char('Delivery Status', compute='_delivery_ratio')
    last_delivery = fields.Datetime('Date of Transfer', compute='_last_delivery')
    has_rfq_delivery = fields.Boolean('Has RFQ Delivery', store=True, default=False)
    is_dropship = fields.Boolean("Is Dropship", default=False, compute='_is_dropship')

    @api.one
    def _delivery_ratio(self):
        total_purchase = 0
        total_received = 0
        status = "Incomplete"

        for line in self.order_line:
            total_purchase += line.product_qty
            total_received += line.qty_received

        if total_purchase == total_received:
            status = "Completed"

        self.arrival_status = status + " " + \
            str(int(total_received)) + " / " + str(int(total_purchase))

    @api.one
    def _last_delivery(self):
        picking_dates = []
        for picking in self.picking_ids:
            picking_dates.append(picking.date_done)

        if(picking_dates):
            self.last_delivery = max(picking_dates)

    @api.multi
    def button_create_picking(self):
        if self.has_rfq_delivery:
            return False

        for order in self:
            if order.state not in ['draft']:
                continue
            order._create_picking()
            self.has_rfq_delivery = True
        return True

    @api.multi
    def button_approve(self, force=False):
        self.write({'state': 'purchase'})

        if not (self.has_rfq_delivery):
            self._create_picking()

        if self.company_id.po_lock == 'lock':
            self.write({'state': 'done'})
        return {}

    @api.one
    def _is_dropship(self):
        order = self.env['sale.order'].search([('name', '=', self.origin)])
        for line in order.order_line:
            if line.route_id.name == 'Drop Shipping':
                self.is_dropship = True

    @api.multi
    def button_confirm(self):
        confirmed = super(PurchaseOrder, self).button_confirm()
        order_lines = self.env['purchase.order.line'].search([('order_id', '=', self.id)])
        stock_moves = self.env['stock.move'].search([('origin', '=', self.name)])

        lines = []
        for line in order_lines:
            lines.append(line.price_unit)

        index = 0
        for move in stock_moves:
            move.write({'price_unit': lines[index]})
            index += 1

        return confirmed
