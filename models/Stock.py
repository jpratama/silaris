# -*- coding: utf-8 -*-

from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'
    display_order = fields.Integer("Display order", default=0)
    _order = 'display_order asc'

class StockPickingType(models.Model):
    _inherit = 'stock.picking.type'
    display_order = fields.Integer("Display order", default=0)
    _order = 'display_order asc'

class StockPicking(models.Model):
    _inherit = 'stock.picking'
    invoice_number = fields.Char('Invoice number', compute='_get_invoice_number')
    is_dropship = fields.Boolean("Is Dropship", default=False, compute='_is_dropship')

    @api.one
    def _get_invoice_number(self):
        if len(self.sale_id.invoice_ids) == 1:
            invoice = self.sale_id.invoice_ids[0]
            self.invoice_number = invoice.number

    @api.one
    def _is_dropship(self):
        warehouse = self.picking_type_id.warehouse_id
        if len(warehouse) < 1:
            self.is_dropship = True