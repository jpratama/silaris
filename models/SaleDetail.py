# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools

class SaleDetail(models.Model):
    _name = 'silaris.sale_detail'
    _description = 'Sales detail based on delivered quantity'
    _auto = False
    _order = 'so_number DESC'

    dn_scheduled = fields.Date('Scheduled Delivery', readonly=True)
    dn_state = fields.Char('Delivery Status', readOnly=True)
    so_number = fields.Char('Sales Order', readonly=True)
    so_state = fields.Char('Sales Order Status', readOnly=True)
    dn_number = fields.Char('Delivery Note', readonly=True)
    inv_number = fields.Char('Invoice', readonly=True)
    inv_date = fields.Date('Invoice Date', readonly=True)
    customer_ref = fields.Char('Customer Ref.', readonly=True)
    customer_name = fields.Char('Customer', readonly=True)
    dn_product_name = fields.Char('Product', readonly=True)
    dn_quantity = fields.Float('Delivered Qty.', readonly=True)
    so_unit_price = fields.Float('Unit Price', readonly=True)
    so_product_sales = fields.Float('Amount Untaxed', readonly=True)
    dn_cost_revenue = fields.Float('Cost of Revenue', readonly=True)
    so_route = fields.Char('Route', readOnly=True)
    sales_person = fields.Char('Sales Person', readOnly=True)
    sales_area = fields.Char('Sales Area', readOnly=True)
    business_unit = fields.Char('Business Unit', readOnly=True)

    # Contoh query for view/report: https://github.com/odoo/odoo/blob/10.0/addons/sale/report/sale_report.py
    def _query(self):
        query_str = """
            SELECT DISTINCT ON(dn.name, dn_line.name, dn_line.product_qty) row_number() over () as id,
                dn.name as dn_number,
                dn.max_date as dn_scheduled,
                dn.state as dn_state,	            
                proc.name as so_number,
                so.state as so_state,
                route.name as so_route,
                dn_line.name as dn_product_name,
                dn_line.price_unit as dn_unit_price,
                so_line.price_unit as so_unit_price,
                dn_line.product_qty as dn_quantity,
                so_line.product_uom_qty as so_quantity,
                (so_line.product_uom_qty - dn_line.product_qty) as dn_todo,
                (dn_line.price_unit * dn_line.product_qty) as dn_cost_revenue,
                (so_line.price_unit * dn_line.product_qty) as so_product_sales,
                inv.number as inv_number,
                inv.date_invoice as inv_date,
                customer.display_name as customer_name,
                so.client_order_ref as customer_ref,
                sales_area.name as sales_area,
                businessunit.name as business_unit,
                partner.name as sales_person

            FROM stock_picking dn
                INNER JOIN stock_picking_type dn_type ON dn.picking_type_id = dn_type.id
                    AND (dn_type.code = 'outgoing' OR dn_type.name = 'Dropship')
                INNER JOIN stock_move dn_line ON dn_line.picking_id = dn.id
                LEFT JOIN procurement_group proc ON dn_line.group_id = proc.id
                INNER JOIN sale_order so ON proc.name = so.name
                -- join quantity yang telah delivered saja
                INNER JOIN sale_order_line so_line ON so_line.order_id = so.id
                    AND dn_line.product_id = so_line.product_id --Bug #47 AND dn_line.product_qty = so_line.qty_delivered)
                INNER JOIN stock_location_route route ON so_line.route_id = route.id
                LEFT JOIN account_invoice inv ON inv.origin = proc.name
                INNER JOIN res_partner customer ON so.partner_id = customer.id
                -- join customer baik yang mempunyai sales area atau tidak
                LEFT JOIN sales_area ON customer.salesarea = sales_area.id
                -- join customer baik yang mempunyai BU atau tidak
                LEFT JOIN account_analytic_account as businessunit ON customer.businessunit = businessunit.id
                INNER JOIN res_users users ON so.user_id = users.id
                INNER JOIN res_partner partner ON users.partner_id = partner.id
            -- more filter for dirty data
            WHERE dn.state = 'done' AND so.state != 'cancel'
        """
        return query_str

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'silaris_sale_detail')
        self._cr.execute("CREATE OR REPLACE VIEW silaris_sale_detail AS (" + self._query() + ")")