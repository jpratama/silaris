# -*- coding: utf-8 -*-

from odoo import models, fields, api

class SaleOrder(models.Model):

    _inherit = 'sale.order'
    delivery_status = fields.Char('Delivery Status', compute='_delivery_ratio')

    @api.one
    def _delivery_ratio(self):
        total_order = 0
        total_delivered = 0
        status = "Incomplete"

        for line in self.order_line:
            total_order += line.product_qty
            total_delivered += line.qty_delivered

        if total_order == total_delivered:
            status = "Completed"

        self.delivery_status = status + " " + \
            str(int(total_delivered)) + " / " + str(int(total_order))

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        super(SaleOrder, self).onchange_partner_id()
        self.project_id = self.partner_id.businessunit