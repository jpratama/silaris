# -*- coding: utf-8 -*-

from odoo import models, fields

class SaleReport(models.Model):
    _inherit = 'sale.report'
    salesarea_id = fields.Many2one('silaris.salesarea', 'Sales Area')
    untaxed_delivered = fields.Float('Untaxed Delivered')
    date_delivered = fields.Datetime('Date Delivered', readOnly=True)

    def _select(self):
        select_str = super(SaleReport, self)._select()
        return select_str + """
            ,partner.salesarea AS salesarea_id
            ,(l.price_unit * l.qty_delivered) AS untaxed_delivered
            ,picking.date AS date_delivered
        """

    def _from(self):
        from_str = super(SaleReport, self)._from()
        return from_str + """ 
            LEFT JOIN silaris_salesarea salesarea ON partner.salesarea = salesarea.id
            JOIN stock_picking picking ON picking.origin = s.name
        """

    def _group_by(self):
        group_by_str = super(SaleReport, self)._group_by()
        return group_by_str + """ 
            ,partner.salesarea
            ,l.price_unit
            ,l.qty_delivered
            ,picking.date
        """