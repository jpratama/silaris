# Silaris
Odoo 10 addons project Silaris.

[Guidelines Odoo 10](https://www.odoo.com/documentation/10.0/reference/guidelines.html)

[Modul directory scaffolding](https://www.odoo.com/documentation/10.0/reference/cmdline.html#scaffolding)

## Struktur Direktori
### Demo
Tidak digunakan.

### Models
Digunakan seperti untuk inherit dan membuat kolom baru pada tabel yang telah ada:

```python
_inherit = 'sale.order'
delivery_status = fields.Char('Delivery Status', compute='_delivery_ratio')
```

atau melakukan perhitungan atau pencarian:
```python
order = self.env[model_name].search([('name', '=', self.origin)])
```
```python
for line in self.order_line:
    total_order += line.product_qty
    total_delivered += line.qty_delivered
```

### Security
Berisi satu file yaitu ```ir.model.access.csv``` untuk mengatur hak akses pengguna terhadap menu dan fungsi dari addons Silaris.

_Jika akses tidak diset, maka user tidak akan dapat melihat atau mengakses menu dari addons_.

Untuk project Silaris, hak akses terhadap menu dan fungsi addons adalah untuk any user yang bisa login ke Odoo.

### Views
Direktori tempat modifikasi tampilan form dan list / grid.

### Views/Templates
Direktori tempat modifikasi print out / PDF seperti invoice, delivery slip dan sebagainya. Penamaan file sebisa mungkin disesuaikan dengan konteks, seperti: ```deliveryslip.xml``` untuk printout delivery slip, ```invoice.xml``` untuk printout invoice dan sebagainya.